class Scroller {
  constructor() {
    this.inScroll = false;
    this.speedPerSecond = 5000;
    this.targetPositionY = 0;
    this.prevTimestamp = 0;
  }

  reset() {
    this.inScroll = false;
    this.targetPositionY = 0;
    this.prevTimestamp = 0;
  }

  scrollTo({ id, delay = 0, additionalOffset = 0 }) {
    if (this.inScroll || !id) return;
    this.inScroll = true;

    setTimeout(() => {
      this.targetPositionY = document.getElementById(id).getBoundingClientRect().top + window.pageYOffset + additionalOffset;
      window.requestAnimationFrame(timestamp => {
        this.scroll(timestamp);
      });
    }, delay);
  }

  scroll(timestamp) {
    const prevTimestamp = this.prevTimestamp || timestamp;
    const delta = (timestamp - prevTimestamp) / 1000;
    const offset = delta * this.speedPerSecond;

    const currentPosition = window.pageYOffset;
    const distance = this.targetPositionY - currentPosition;

    const newPosition = distance < 0 ? currentPosition - Math.min(offset, -distance) : currentPosition + Math.min(offset, distance);
    window.scroll(0, newPosition);

    if (window.pageYOffset === currentPosition && this.prevTimestamp) {
      this.reset();
    } else {
      this.prevTimestamp = timestamp;
      window.requestAnimationFrame(newTimestamp => {
        this.scroll(newTimestamp);
      });
    }
  }
}

const scroller = new Scroller();
export { scroller };
